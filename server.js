'use strict';

const Hapi = require('hapi');

// Create a server with a host and port
const server = new Hapi.Server();
server.connection({ 
    host: 'localhost', 
    port: 8888 
});

// Add the route
server.route({
    method: 'GET',
    path:'/add/{number}', 
    handler: function (request, reply) {
        var number = parseInt(request.params.number, 10);
        var operation = number + number;
        var result = number + ' + ' + number + ' = ' + operation;
        return reply(null, result);
    }
});

// Start the server
server.start((err) => {

    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});